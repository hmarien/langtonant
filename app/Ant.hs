{-# LANGUAGE TemplateHaskell #-}

module Ant where

import Control.Lens

data Ant = Ant { _position  :: Point,
                 _direction :: Direction }

data SColor = Black | White | Red | Green | Blue | Yellow | Magenta | Cyan | Rose | Violet | Azure | Aquamarine | Chartreuse | Orange deriving (Eq, Ord)

type Point = (Int, Int)
type Direction = (Int, Int)
type Action = Direction -> Direction

makeLenses ''Ant

turnAnt :: Action -> Ant -> Ant
turnAnt action = direction %~ action

getAntPosition :: Ant -> Point
getAntPosition = flip (^.) $ position

moveAnt :: Ant -> Ant
moveAnt state = position %~ changePosition $ state
  where
    changePosition (x, y) = ( x+dx, y+dy )
    (dx, dy) = state ^. direction


-------------
-- ACTIONS --
-------------

l :: Action
l = turnLeft

turnLeft :: Action
turnLeft dir = newdir
  where
    (x, y) = dir
    newdir = (-y, x)

r :: Action
r = turnRight

turnRight :: Action
turnRight dir = newdir
  where
    (x, y) = dir
    newdir = (y, -x)
