{-# LANGUAGE TemplateHaskell #-}

module Program where

import Control.Lens
import Graphics.Gloss hiding (Point)
import Data.Function
import Field
import Ant
import qualified Data.Map.Strict as M
import Data.Maybe

data State = State { _field :: Field, _ant :: Ant, _pixelsize :: Float, _actionMap :: M.Map SColor Action, _colorCycle :: ColorCycle, _numberOfSteps :: Int }

makeLenses ''State

-------------------
-- STATIC VALUES --
-------------------

--The initial empty field
initialField :: Field
initialField = M.empty

--Initial ant state
initialAnt :: Ant
initialAnt = Ant { _position = (0,0), _direction = (0,1) }

-------------
-- DRAWING --
-------------

--Draw function, draws state (field, ant)
drawState :: State -> Picture
drawState state = pictures [drawField s f, drawAnt a]
  where
    f = state ^. field
    a = state ^. ant
    s = state ^. pixelsize

--Draws the field
drawField :: Float -> Field -> Picture
drawField size field = pictures . fmap (drawCell size) . getFieldData $ field

--Draw ant (the ant moves to fast to properly see the ant, so you can ommit it)
drawAnt :: Ant -> Picture
drawAnt _ = pictures []

--Draw 1 cell (coordinate)
drawCell :: Float -> ((Int, Int), SColor) -> Picture
drawCell size ((x', y'), c) = Color color $ Translate (size*x) (size*y) $ rectangleSolid size size
  where
    color = toColor c
    x = fromIntegral x'
    y = fromIntegral y'

toColor :: SColor -> Color
toColor White = white
toColor Black = black
toColor Red   = red
toColor Green = green
toColor Blue  = blue
toColor Yellow  = yellow
toColor Magenta = magenta
toColor Cyan    = cyan
toColor Rose       = rose
toColor Violet     = violet
toColor Azure      = azure
toColor Aquamarine = aquamarine
toColor Chartreuse = chartreuse
toColor Orange     = orange

--------------------
-- STATE CHANGING --
--------------------

--Compute the next step/state
nextState :: State -> State
nextState state = repeatf step n $ state
  where step = moveAnt_State . updateColor_State .  turnAnt_State . checkCoordinate_State
        n = state ^. numberOfSteps

--Repeatidly apply the same function a number of times
repeatf :: (a -> a) -> Int -> (a -> a)
repeatf _ 0 = id
repeatf f n = (repeatf f (n-1)) . f

--Check the existence of current coordinate (and add+initialise if not)
checkCoordinate_State :: State -> State
checkCoordinate_State state = field %~ checkCoordinate colorcycle point $ state
  where colorcycle = state ^. colorCycle
        point = getAntPosition $ state ^. ant

--Turn the ant
turnAnt_State :: State -> State
turnAnt_State state = ant %~ turnAnt action $ state 
  where action   = fromMaybe (undefined) $ M.lookup color (state ^. actionMap)
        color    = getColor position $ state ^. field
        position = getAntPosition (state ^. ant)

--Update the color at the ants position
updateColor_State :: State -> State
updateColor_State state = field %~ updateColor point $ state
  where
    point = getAntPosition $ state ^. ant

--Move the ant
moveAnt_State :: State -> State
moveAnt_State = ant %~ moveAnt

----------
-- MAIN --
----------

start :: Int -> Int -> Float -> String -> IO ()
start n k s a = simulate window bgcolor fps initialState drawingFunction (\_ _ -> stateFunction)
  where
    window  = InWindow "Langton's Ant" (400,400) (0,0)
    bgcolor = greyN 0.85
    fps           = n
    pixelSize     = s
    actionString  = a
    numberOfSteps = k
    initialState    = State {_field = initialField, _ant = initialAnt, _pixelsize = pixelSize, _actionMap = actionMap, _colorCycle = colorCycle, _numberOfSteps = numberOfSteps}
    drawingFunction = drawState
    stateFunction   = nextState
    actionMap  = makeActionMap a
    colorCycle = makeColorCycle a

--maps colors to actions
makeActionMap :: String -> M.Map SColor Action
makeActionMap a = M.fromList $ zip colorlist (actionlist a)

--cycles a set of colors
makeColorCycle :: String -> [SColor]
makeColorCycle s = cycle $ take (length s) colorlist

--parses the input into actions
actionlist :: String -> [Action]
actionlist []     = []
actionlist (a:as) =
  case a of
    'r' -> r: actionlist as
    'R' -> r: actionlist as
    'l' -> l: actionlist as
    'L' -> l: actionlist as

--possible colors (actionmapping with more actions than this list will reuse colors)
colorlist :: [SColor]
colorlist = [White, Red, Green, Blue, Yellow, Cyan, Magenta, Rose, Violet, Azure, Aquamarine, Chartreuse, Orange, Black]


