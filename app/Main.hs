module Main where

import Program

main :: IO ()
main = do
  putStrLn "Insert speed (fps):"
  speed <- getLine
  putStrLn "Number of steps (each frame):"
  number <- getLine
  putStrLn "Insert blocksize (in pixels):"
  size <- getLine
  putStrLn "Insert actionCycle (l/L, r/R):"
  actionString <- getLine
  start (read speed) (read number) (read size) (actionString)
