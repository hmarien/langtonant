module Field where

import Graphics.Gloss.Data.Color
import qualified Data.Map.Strict as M
import Ant
import Data.Bifunctor

--colorcycles are infinite lists
--The head of this list denotes the current color
--You can cycle through colors by repeatingly removing the head of this list
type ColorCycle = [SColor]

--each point on the map has a color(cycle)
type Field = M.Map Point ColorCycle

--If the point is new, create a new mapping with a fresh colorcycle
--if the point already exists do nothing
checkCoordinate :: [SColor] -> Point -> Field -> Field
checkCoordinate colorlist point field =
  case M.lookup point field of
    Just colorCycle -> field
    Nothing         -> M.insert point (cycle colorlist) field

--Removes the head of a colorcycle
updateColor :: Point -> Field -> Field
updateColor point field =
  case M.lookup point field of
    Just colorCycle -> M.adjust tail point field
    Nothing         -> field

--looks up the current color of a point (defaults to white if nothing is found)
getColor :: Point -> Field -> SColor
getColor point field =
  case (M.lookup point field) of
    Just cycle -> head cycle
    Nothing    -> White --should never happen, how can i guarantee this?

--get a list of coordinates with their respective colors
getFieldData :: Field -> [((Int, Int), SColor)]
getFieldData field = map (bimap id head) $ M.toList field
